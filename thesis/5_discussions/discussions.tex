\section{Discussions} \markboth{Discussions}{Discussions}

\subsection{Contributions}
The outcome of this PhD project is a set of research contributions related to
word and tag clouds generation from social media data. We systematically present individual contributions with respect to two dimensions
(introduced in Section \ref{sec:problemstatement}) which define the research
scope of this PhD project. We recall the organization of research questions and
their corresponding relationships to the dimensions in the following Table
\ref{tab:resqcontributions}. We believe that this classification provides a
complete picture of individual contributions as well as bounds the research
scope of this doctoral thesis.
\begin{table}[h]\begin{center}
    \begin{tabular}{ |  p{4cm}| c | p{4cm} |}
    \hline
    Types of social media data / Challenges & {\bf{Folksonomy}} & {\smd} 
    \\
    \hline {\bf{Relevance}}  &  RQ1a & RQ1b \\ \hline
    {\bf{Redundancies which lead into decreased diversity}} &  RQ2a & RQ2b \\
    \hline {\bf{A difficult interpretation of visualized tags or terms}} &  & 
    RQ3\\\hline
    \end{tabular}
    \caption{Research questions classification with respect to both dimensions
    defined in the problem statement.}
    \label{tab:resqcontributions}
    \end{center}
    \end{table}
In the following paragraphs, we present individual contributions that follow
the presented classification.
\subsubsection{Improving relevance of word and tag clouds}
When addressing the first research question {\bf RQ1 (How to generate more
relevant tag and word clouds?)}, we show that popularity or coverage oriented
tag clouds often contain many terms which are irrelevant for users (see Motivation
section in research paper {\bf{P2}} in Appendix \ref{app:p2}). Therefore,
there is a need to generate clouds with relevant tags or terms. It is difficult to
define and measure relevance without a concrete reference i.e., a user
information need. Hence, when aiming to improve Relevance of tag and word
clouds, we explore two research subquestions: {\bf RQ1a: How to generate more
relevant tag clouds with respect to the user query?} and {\bf RQ1b: How to generate personalized word clouds with respect to user preferences?} Both
research questions aim to provide more relevant clouds for a
user (enhanced clouds should help with the retrieval of relevant resources and
in the end satisfy a user information need).
However, there are different arrangements for how a user information
need is expressed as well as what kind of underlying data are used for a cloud
generation. 

\paragraph{Contributions to RQ1a}
When generating tag clouds with respect to a user query, the state-of-the-art methods often select irrelevant tags. The contribution
of this PhD project is an approach that exploits graph-based algorithms for more
relevant tag clouds when generated with respect to a user query. To perform graph-based ranking of tags with respect to a user query, a tag space is transformed into a graph. The graph transformation is obtained through
co-occurrence calculations between individual tags. Tags that are frequently
co-occurring are considered as similar.  Consequently, an edge between similar
tags is introduced. We exploit different graph-based ranking methods for
relevance estimation of tags with respect to a user query. Due to excessive
computational demands, we find distance graph-based approaches (shortest paths
\cite{white2003algorithms}) unsuitable for this task. Conversely, the stochastic
approaches which simulate a random traversal of the graph are less
computationally complex and attain significantly better relevance than
state-of-the-art methods \cite{venetis2011selection}. Relevance is improved by $41\%$ on Movielens dataset and $11\%$ on BibSonomy when compared with the state-of-the-art selection technique (see details in research paper {\bf{P2}} in Appendix \ref{app:p2}). Further, when generating tag clouds with respect to multiple
keywords query, graph-based methods perform the best (see research
paper {\bf{P3}} in Appendix \ref{app:p3}). When encoding a multiple keywords
query into a prior probability vector which introduces ``bias'' towards the query into the graph-based
ranking, we propose encoding keyword tags from the query according to their
relative popularity (relative in the context of query tags). This is motivated
by the large differences in relative tags popularities among considered query
tags (see details in {\bf{P3}} in Appendix \ref{app:p3}). We show that the
graph-based approach enhances relevance of tag clouds. Further, the approach
provides the means for intuitive integration of user information retrieval
needs e.g., a user query, multiple keywords query or user profile.

To further ensure that generated tag clouds are relevant, we propose to explore
available indicators from underlying data as signals for whether resources that
can be found through a tag cloud are relevant. The relevance indicators derived from underlying data reflect an opinion of other users from a folksonomy system.
For instance, on Movielens, we exploit average movie rating for a movie as an
indicator of relevance. Hence, when users rate the given movie as relevant, it
is more likely that the given user will also find it relevant. 

\paragraph{Contributions to RQ1b}
The research question {\bf RQ1b (How to generate personalized word clouds with
respect to the user preferences?)}, tackles a problem of an information
overload in {\smd}. Users of social media systems are
often overwhelmed with the vast amount of irrelevant resources. To minimize the burden
of an information overload, word clouds are often used as a browsing interface
for the underlying data (see Section \ref{sec:informationoverload}).
State-of-the-art word cloud generation methods present the most important terms
from the underlying collection; however, it is not clear whether these terms link to
resources that are relevant to the user. In research paper {\bf{P4}} (see
Appendix \ref{app:p4}), we focus on the generation of personalized word clouds.
These personalized word clouds should present terms from an underlying data
collection that are {\bf{relevant}} with respect to user preferences. 
Similarly as in {\bf RQ1a}, we show that graph-based methods generate
personalized word clouds and provide the means for convenient integration of
user preferences to bias ranking towards content relevant for the user. A new
strategy of improving word cloud generation for Twitter users is proposed. The strategy employs and combines
different user past information including: tweets written by a user,
retweeted tweets and ignored tweets. We consider tweets, which were seen
but not retweeted and at the same time were displayed close to retweeted tweets, as
ignored tweets. Ignored tweets represent a negative user preference. We
systematically evaluate individual as well as a combination of user past
information. The most optimal combination of user past information with negative
user past information (ignored tweets) is attained through the combination of 
graph-based rankings (Post-ranking combination denoted as RC). The RC method
outperforms the approach combining the term weights first and then feeding them
into the graph-based algorithm (see more details in {\bf{P4}} in Appendix
\ref{app:p4}).

The main findings from {\bf{P4}} are: (1) The retweeted tweets are
more useful than tweets written by the user when generating personalized word clouds;
(2) The ignored tweets can improve further the relevance of word clouds; (3) A
combination of all user past information attains the best relevance. Another
contribution is a novel evaluation methodology for word clouds. The
evaluation considers a word cloud as a weighted query. The assumption is that
word clouds that retrieve more relevant tweets with respect to the query i.e.,
word cloud, are better.

 %  {\bf RQ1: How to generate more relevant tag and word clouds?} 
%  The main aim is to generate word clouds with relevant tags or terms such that
% users can find, browse and explore relevant documents from the underlying
%  collection. We define two following sub-questions:
\begin{comment}
\begin{itemize}
      \item {\bf RQ1a: How to generate more relevant tag clouds with respect to
    the user query?} Often, there is a need to generate tag cloud with respect
    to a user search query. The aim is to generate tag clouds with relevant
    and discriminative tags instead of depicting the popular but general tags which
    are useless for the users.
    \item {\bf RQ1b: How to generate personalized word clouds with respect to
    the user preferences?} When user past activities in a social media system are
    available, a word cloud generation can be enhanced and biased towards user
    preferences. The goal is to investigate how user past activities can be
    exploited to generate more personalized word clouds?
 \end{itemize}
 \end{comment}

 \subsubsection{Decreasing redundancies in word and tag clouds}
 Social media systems accumulate user-generated content. Naturally, when a large
 number of users participate in the content creation, redundant
 information is introduced into systems. In research question {\bf{RQ2}
 (How to minimize redundant tags and words from the tag clouds to increase diversity of terms in the cloud?)}, we aim to group syntactical and
 semantical variations of tags, entities and terms which in consequence should
 lead to greater diversity of generated clouds. Indeed, we show that
 clustering can positively improve tag and word cloud generation. In the
 following, the contributions are presented with respect to two research sub-questions:
 {\bf RQ2a (How to cluster syntactically and semantically similar tags
 to generate more diverse tag clouds?)} and {\bf RQ2b(How to group entities
 variations to generate enhanced word clouds?)}.
 
 \paragraph{Contributions to RQ2a}
 We propose to employ different clustering algorithms and envision how they can
 be exploited when generating tag clouds (see details in research paper
 {\bf{P1}} in Appendix \ref{app:p1}). In particular, clustering techniques
 facilitate the reduction of redundant tags in two ways: (1) Grouping of
 syntactical variations of tags e.g., singular and plural forms of tags,
 misspellings as well as compounded tags where two terms are concatenated with
 some separator; (2) Grouping of tags synonyms which refer to the same entity,
 topic etc.
 
 To group syntactical variations of tags, we leverage Levenshtein string
 distance to filter out tags with misspellings, plural and singular forms of the
 tag and compounded tags with the same meaning. For instance, on Delicious, tags
 like:  {\emph{Web\_design, web\-design, webDesign or *webdesign}} can be
 grouped and represented only with {\emph{webdesign}}. Levenshtein distance
 captures a number of required edits - insertion, substitution or deletion to transform
 one tag into another. It is calculated for each tag pair from the original tag
 space. Clusters are formed with tags among which the distance is equal or
 lower than a chosen threshold. Consequently, each cluster is represented with
 the most frequent tag. When compared to the baseline method, a popularity-based
 selection, and the baseline enhanced with syntactically clustered tags, we find that Coverage is improved. The improvements of Coverage are almost $5\%$ (5079 research
 publications) on BibSonomy and $3.5\%$ (3072 documents) on Delicious. It is
 worthy to mention that Overlap of tag clouds remains the same as for the
 baseline. It means that tag clouds with syntactical clustering cover more
 resources and at the same time measured redundancies are not increased. 
 
 To further enhance tag clouds quality, we group similar tags to
 decrease semantic redundancies in the tag cloud. Formed clusters with
 (semantically) similar tags represent individual topics from the tag space. The
 proposed methodology selects tags from clusters proportionally to the
 popularity of the cluster within the folksonomy. The proportional selection of
 tags from clusters is important, as the majority of clusters is less popular
 than a few popular clusters (long tail distribution of topics in folksonomies). The best
 performing clustering method is a complete linkage agglomerative
 hierarchical clustering. This clustering method enhances average coverage
 by $12.4\%$ on BibSonomy and $3\%$ on Delicious. Moreover, the methodology
 decreases overlap on both datasets (see {\bf{P1}} in Appendix \ref{app:p1}). 
 

An additional benefit of leveraging clustering for tag cloud generation is
the possibility to visualize tags from the same cluster with the distinct color
or position which allows the comprehension of relationships among tags by users. 

\paragraph{Contributions to RQ2b}
We propose to group recognized named entities and represent them with the
canonical name of the entity. At first, the method leverages a named entity
recognition tool to detect present entities in the corpus. Secondly, we exploit
linked data, in particular from the Freebase repository. For the recognized
entity, we retrieve existing aliases (using {\bf{aliases}} field).
Consequently, retrieved aliases and recognized entities are used to find matching mentions of the
entity. The entity in the final word cloud is represented with the canonical
name. The grouped named entities, when combined with the appropriate word cloud
generation method, significantly improve Coverage and
significantly decrease Overlap. Further, grouped named entities facilitate
improved access to relevant tweets. Hence, word clouds with grouped names
entities cover more tweets from the corpus and at the same time enhance
diversity of the word cloud. The synthetic evaluation is supported with the
findings of the user study. The participants of the user study found word clouds
with grouped named entities more relevant (p = $0.00062$, one sample t-test)
and diverse (p = $0.003$, one sample t-test). Further, the user study points out
the importance of the synthetic relevance evaluation, which is measured with
Mean Average Precision (proposed in research paper {\bf{P4}} in
Appendix \ref{app:p4}). Users find word clouds with higher levels of MAP as more
relevant than the word clouds generated with the baseline technique.  



\begin{comment}
 Social media data are created in the collaborative fashion by different users which leads into many syntactical and semantical variations of tags,
 entities and terms. The aim is to investigate how to minimize such redundacies to allow users explore more diverse content. Again, we attempt
    to tackle two research sub-questions:
\begin{itemize}
    \item {\bf RQ2a: How to cluster syntactically and semantically similar tags
    to generate more diverse tag clouds?} The goal is to cluster syntactical
    variations of tags i.e., typos, singular and plural forms and compounded
    tags that share the same semantical meaning and represent them with the
    canonical tag. Further, we investigate how to group tags synonyms such that
    a more diverse tag cloud is generated?
    \item {\bf RQ2b: How to group entities variations to generate enhanced
    word clouds?} Users exploiting word clouds from {\smd},
    prefer word clouds with recognized named entities \cite{finin2010annotating}. However, users
    tend to create content with different variations and aliases of entities which might decrease the quality of word clouds. The
    aim is to investigate how to group variations of named entities and
    represent them with the canonical entity so that more relevant and diverse word clouds are
    generated.
\end{itemize} 
 \end{comment}
 
\subsubsection{Ad hoc topic maps for enhanced exploration of social media data}
Despite the benefits and simplicity of word and tag clouds, often there is a
need for more complex information retrieval interfaces (see Section
\ref{sec:otherinterfaces}). In research question {\bf{RQ3} (How to visualize tags and terms such that a certain direction or position in the interface is understandable by the user?)}, the emphasis
is on how to facilitate the understanding and interpretation of a position
within a topic map for a user as well as the possibility of supporting
visualization of various meta dimensions of underlying data. We believe that research efforts in this direction might open up a new paradigm of information interaction and lead to
the emergence of useful tools for many users to interact with social media data. 
The contribution of this PhD project with respect to {\bf{RQ3}}, is a novel
two-dimensional topic map called Beomap. It allows defining the ad hoc second
dimension with the keyword queries. This feature enables a more meaningful
interpretation of visualized topics i.e., topics more relevant to the main
query are positioned higher on the vertical axis whereas hashtags more relevant
to the second ad hoc dimension are located further on the horizontal axis (see
details and the screenshot of the topic map in research paper {\bf{P6}} in
Appendix \ref{app:p6}). Beomap with the second ad hoc dimension enables users to
steer browsing and exploration of underlying information space by placing ad hoc
queries into both topic map dimensions. An additional benefit is the ability to support various visualization metrics such as relevance, popularity, recency, proximity, etc. making it easy to do visual analytics. The benefits of Beomap are confirmed with the simulated user interaction with search results. The
results indicate that Beomap improves the retrieval of relevant additional
tweets (tweets relevant but not present within the result set retrieved by the original
query).
In particular, Beomap is useful for scenarios when a user is interested in the
missing aspect of the original query (the missing aspect is used as an input
into the second ad hoc dimension of Beomap). Further, we conducted a user
study. Beomap was integrated into a web retrieval system BeomapApp and compared
with the standard search interface. Participants perceive BeomapApp as more
useful and are more satisfied with it than with the baseline. The system is
perceived as a flexible tool for exploration, browsing and analysis of Twitter
data as well as being easy to browse and interesting.

%It is not clear what a certain direction or position in the tag cloud means
%e.g., what does it mean when a topic is presented in the top right corner
%of the interface. Further, how to enable a user to reach ad hoc relevant
%topics regions in a 
%exible way i.e., user-driven exploration
\subsection{Research Limitations}
While several contributions are made when answering the defined research
questions of this PhD project, we also recognize research limitations. The
acknowledged research limitations provide a more fair and objective picture of
the overall PhD project as well as provide the basis for future research and
development for the topic of this thesis. We present limitations with respect to
individual research questions.

\subsubsection{Limitations when answering RQ1a}
When addressing research question {\bf{RQ1a}}, we find graph-based tag cloud
generation as a useful means to enhance the relevance of tag clouds. However,
the optimal graph-based estimation of tags relevance depends on correctly tuned
parameters of the algorithm. For instance, a similarity threshold $\alpha$
determines which tag pairs are connected with edges in the graph (see
details in research paper {\bf{P2}} in Appendix \ref{app:p2}). Further, a
back probability $\beta$ parameter as well as an encoding of the prior
probability distribution (see {\bf{P3}} in Appendix \ref{app:p3}) have an effect
on the final tag and word cloud generation. Hence, the graph-based tag cloud
generation requires a parameters tuning process. In addition, due to changes in
the underlying data, there is possibly a need to repeat parameter optimization
over time. To minimize the negative aspects of the parameters dependence,
various state space search algorithms might be leveraged to simplify the parameters tuning.

In research paper {\bf{P2}} (see Appendix \ref{app:p2}), we propose exploiting
relevance indicators derived from the underlying data. The assumption with binary relevance i.e., that a resource is either relevant or irrelevant,
is often unrealistic. For instance, on BibSonomy dataset, we consider each
research publication relevant if it has at least one citation. Similarly, on Movielens we consider the movie relevant if it attained average ratings
greater than $2.5$ (mean of the rating scale). The binary relevance
classification of resources might not potentially capture the subtle relevance
differences among resources. To address the binary assumption, continuous
relevance scales should be utilized.  

\subsubsection{Limitations when answering RQ1b}
\label{limitations:rq1b}
To perform personalized word cloud generation, there is a need for explicit
indicators of user preferences e.g., what is relevant for a
particular user. In research paper {\bf{P4}} (see Appendix \ref{app:p4}), a
retweet action of a user indicates whether a particular tweet is relevant or interesting for the user.
Conversely, \cite{rout2013reliably} reports that retweets are not always
reliable indicators of relevance. Rout et al. \cite{rout2013reliably} find that
only $66\%$ of tweets which were explicitly classified as relevant were
retweeted by any Twitter user at all. Often Twitter users do not retweet tweets
which are part of their personal communication as it is meaningless to share such tweets with the user
followers. Although, retweets are often leveraged as relevance indicators on
Twitter, a user study should be performed. The user study would enable the
acquisition of explicit relevance ratings and consequently, a more reliable
evaluation of personalized word cloud generation could be realized.

The introduced evaluation methodology for personalized word clouds proposed in
research paper {\bf{P4}} (see Appendix \ref{app:p4}) employs standard
state-of-the-art retrieval function OKAPI BM25 (see Section
\ref{sec:scoringfunctions}) when measuring mean average precision of individual
word clouds. We justify the retrieval function choice because of the following:
(1) OKAPI BM25 is an established and recognized retrieval function in the
research community; (2)The function is supported by modern information retrieval
systems like Lemur, Apache Solr or ElasticSearch, which provides the means for
other researchers to compare results; (3) It is computationally inexpensive.
Despite the benefits of OKAPI BM25, more customized retrieval functions towards
the specific nature of Twitter data could be employed i.e., tweets are limited
to $140$ characters. There is an intensive research activity about information retrieval
on social media data, hence when a more optimal scoring function is
developed and becomes standard, it could replace OKAPI BM25 in the evaluation
methodology.

%-- word cloud generation from terms

\subsubsection{Limitations when answering RQ2a}
As shown in research paper {\bf{P1}} (in Appendix \ref{app:p1}), cluster
analysis enhances tag cloud generation.  The performed evaluation is based on
synthetic metrics like Coverage and Overlap, which capture certain aspects of
tag cloud quality. However, to validate completely the benefits of cluster
analysis for tag cloud generation, a user study with the evaluation of clustering techniques and their impact on tag cloud generation
should be performed. Further, the leveraged clustering techniques perform so
called ``hard clustering'' which assigns each tag to exactly one cluster. The
drawback is when tags with multiple meanings are present i.e., polysemy of tags.
In such scenarios, a tag with multiple meanings might be assigned to the cluster
that is related to the most prevalent meaning of the tag in the folksonomy. As a
consequence, it potentially might lead to decreased accuracy of clustering
as well as to decreased precision of information retrieval through the generated
tag cloud. To overcome this drawback, some soft-based clustering techniques
should be leveraged e.g., fuzzy clustering. 

\subsubsection{Limitations when answering RQ2b}
Grouped named entities enhance word cloud generation from social media data.
However, the improvements are bound by the accuracy of leveraged named entity
recognition tools. In research paper {\bf{P5}} (see Appendix \ref{app:p5}),
the employed named entity recognition tool introduced two types of errors into
the process of word cloud generation: (1) False positives of recognized entities
introduce noise into the word cloud e.g., a word cloud generated with respect
to the query {\emph{Super Bowl, seats}} contained {\emph{Super (2010 American
film)}} which is irrelevant for the query; (2) Incorrect grounding of ambiguous
named entities leads to inclusion of several semantically related entities
to a word cloud which causes lower diversity e.g., {\emph{BBC News Service}} links to
several semantically similar entities such as {\emph{BBC, BBC News}} and
{\emph{BBC NEWS Service}} which causes more redundancies in word clouds. To
address the latter challenge, we diversify word cloud generation with the
DivRank algorithm. However, more accurate named entity recognition tools need to
be developed to overcome the presented limitations.

Further, the performed crowdsourcing evaluation might be questioned due to
the uncertainty of whether acquired user ratings are reliable. To overcome this
uncertainty, we aimed to collect more user ratings and observe the
final aggregated ratings.

\subsubsection{Limitations when answering RQ3}
Beomap, a two-dimensional topic map proposed in research paper {\bf{P6}} (in
Appendix \ref{app:p6}) visualizes Twitter topics which are defined by users as
hashtags. The limitation of hashtags is their sparse nature within the
corpus i.e., only $24\%$ of tweets from TREC2011 collection contain a hashtag.
Hence, a topic map depicting only hashtags might cover only a limited fraction
of relevant tweets. Therefore, to overcome this limitation, there is a need for
annotation as well as named entity disambiguation tools, which will help to
generate possible representative topic keywords for tweets. Further, validated
benefits of Beomap through simulated user behavior as well as a task-oriented
user study should supplement the long-term user study. Such a long-term user
study could potentially reveal benefits as well as drawbacks of Beomap in more realistic conditions.

\subsection{Future work}
This PhD project tackles how to enhance tag and word clouds generation to
improve user browsing of underlying social media data. The research
contributions made in this project could be perceived also as the basis for
further research work. In the following, we present several open problems with respect to the research questions of this
project. 
When improving relevance of tag and word clouds ({\bf{RQ1}}), we would like to
investigate different graph-creation techniques, which would go beyond
co-occurrence similarities between tags. Further, the emphasis of the future
work would be on a rigorous and systematic definition of relevance, how to
derive it from social media meta data, etc. For personalized word cloud generation, we
would like to leverage also non-lexical information which are available on
Twitter e.g., hashtags, social network properties of the user, etc.  It would be
interesting to rigorously study the relationship between enhanced word cloud
generation methods proposed in this PhD project to different pagination
algorithms \cite{helic2011tag}.

When tackling the challenge of redundancies within social media data and its
effects on tag and word cloud generation ({\bf{RQ2}}), we would like to leverage
soft clustering methods to avoid the problem of terms polysemy (e.g., fuzzy
c-means \cite{bezdek1984fcm}). Further, state-of-the-art NLP techniques should be explored and applied to the problem of
word cloud generation from social media data. 

In line with research question {\bf{RQ3}}, we would like to extend the Beomap
system with the other visualization metrics such as sentiment, authority and
personalized metric. Further, the goal is to perform a long-term study to
observe the behavior of users and to recognize the pros and cons of the topic
map.
