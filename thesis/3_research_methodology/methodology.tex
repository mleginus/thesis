\section{Research methodology}\markboth{Research methodology}{Research methodology}
The majority of the research conducted in this PhD project followed a
constructive research approach \cite{crnkovic2010constructive}. The
objective of constructive research is to invent and to develop a novel
construct.
A construct is usually not discovered as it is created in a distinct way from
anything that existed before. What motivated us to follow the constructive
research approach (CRA) is presented below:
\begin{itemize}
  \item CRA focuses on real world problems that need resolution in
  practice, not just theory. The topic of this thesis e.g., word and tag cloud
  generation from social media data is a real world problem. The research questions raised in
  this PhD project tackle limitations and constraints of tag clouds which have
  been integrated into popular social media systems.
  \item The outcome of CRA is a novel construct, which solves the initial real
  world problem. The developed constructs (contributions) in this PhD
  project (e.g., tag selection algorithms, synthetic evaluation metric and
  intelligent user interface) are novel with respect to existing relevant constructs as well as the benefits of the developed constructs being validated with real word datasets.
  \item Not only is it explicitly bound to the previous related work, but also
  the outcomes of CRA are transformed back to theory \cite{lukka2000key}.
  This PhD project and all individual papers are positioned with respect to the
  existing related work. Further, these published papers may be considered
  theoretical contributions to research areas of information retrieval and social media data visualization.
\end{itemize}
A constructive study usually consists of empirical validation whether or not a
novel construct works consequently lead to a theoretical contribution.
Similarly, in this PhD project, all individual papers contain empirical studies,
which attempt to call attention to benefits of the new constructs e.g.,
graph-based tag generation methods, personalized algorithms for word cloud generation etc.

In the following, we present a constructive research process, which can be
defined in the following steps \cite{lukka2000key}:
\begin{enumerate}
  \item {\em{Identify a practical relevant problem, which also has the potential
  for a theoretical contribution}}. This PhD project tackles an important
  practical problem of how to facilitate browsing, searching and exploring large
  amounts of social media data with word and tag clouds. Further, the research
  related to word and tag cloud generation was premature when this PhD project
  started. Hence, there was room for possible theoretical contributions. At the
  beginning of this PhD project, we identified several subproblems as part of
  the PhD plan document (see Section \ref{sec:resque}).
  \item {\em{Acquire a deep understanding of the problem topic both practically
  and theoretically}}. When tackling each research question (see Section
  \ref{sec:resque}), we explored related relevant research which represented a
  ``starting point'' when answering a research question. To acquire a practical
  understanding of the problem, we implemented relevant existing
  algorithms as well as obtained relevant data collections (see details in
  Section \ref{sec:methodologydatasets}) where the challenge of the raised research question can be observed.
  \item {\em{Innovate a solution idea and propose a problem solving
  construction, which might lead to potential theoretical contributions.}}
  To generate an innovative problem-solving construct, we organized
  brainstorming sessions and, we looked for inspiration within other research
  fields. Further, we performed prototyping to identify quickly possible
  (dis)advantages of the construct.
  \item {\em{Implement the construct and test how it performs.}} Once the
  solution idea was identified, we proceeded to implement the
  construct. Usually, the construct was a tag cloud generation algorithm, which
  was implemented with a certain programming language (mostly Java).
  Consequently, the algorithm was evaluated and compared to existing
  constructs on top of relevant datasets (see details in Section
  \ref{sec:methodologydatasets}). To evaluate the constructs we leveraged
  established synthetic metrics as well as performed user evaluations (see details in Section \ref{sec:methodologymetrics}).
  \item {\em{Reflect on the possible scope of applicability of the
  construct.}} After the empirical evaluation of the individual
  construct, we analyzed, reflected and discussed obtained results. We attempted
  to generalize the findings and propose other possible applications of the
  construct. However, we also attempted to identify possible drawbacks and
  limitations of the construct.
  \item {\em{Identify and analyze the theoretical contribution.}} The created
  construct and performed empirical results were analyzed and inspected in order
  to identify theoretical contributions with respect to the existing related
  research. Once such contributions were identified and we considered them
  sufficient, in a publication we described the new construct, the results of
  empirical evaluation as well as theoretical contributions with respect to existing work.
\end{enumerate}
The contributions of this PhD project are several constructs, presented in
individual papers. In most cases, the construct development as well as the
research paper followed the above-described research process steps.

In the following, we present leveraged datasets as
well as employed evaluation metrics for assessment of individual constructs. The
datasets and metrics provide a means for systematic evaluation of constructs
with respect to existing solutions.
%http://www.metodix.com/en/sisallys/01_menetelmat/02_metodiartikkelit/lukka_const_research_app/03_konst_tut_prosessi
%\item If the research is intented to be performed with the target
% organization
\subsection{Evaluation datasets}
\label{sec:methodologydatasets}
We evaluate the proposed constructs (i.e., tag selection techniques, clustering
methodologies or a two-dimensional topic map) on top of established folksonomy
datasets as well as different tweets collections.
In research paper {\bf{P1}} (see Appendix \ref{app:p1}), we use a BibSonomy
dataset \cite{bibsonomyDataset} and the snapshot of a Delicious dataset which
captures the bookmarking activities on Delicious from $8^{th}$ to $16^{th}$ of
September, 2009. The BibSonomy dataset contains 5,794 distinct users, 802,045
items and 204,850 tags. The total number of tagging posts is 2,555,080. The
snapshot of the Delicious dataset contains 187,359 users, 185,401 unique tags
and 355,525 bookmarks. The total number of tagging posts is 2,046,868. In
research paper {\bf{P3}} (see Appendix \ref{app:p1}), we used BibSonomy and
Movielens datasets. The Movielens dataset contains 16,518 unique tags and 7,601
movies. The total number of tagging posts is 95,580. To be able to derive
relevance indicators for both datasets, we retrieved a citation number using
Microsoft Academic Search for each resource on BibSonomy. Similarly, we calculated an average movie rating assigned by users for a movie.

In research paper {\bf{P4}} (see Appendix
\ref{app:p4}), we employ the same Twitter dataset as in \cite{lage2014vector}.
We obtained the dataset between September 17 to
October 16, 2011. For each user we reconstructed a timeline
consisting of the tweets from users he/she follows. Tweets that were retweeted by a user are considered relevant and thus utilized for the
evaluation of personalized word cloud generation.
This boolean relevance indicator provides a rough insight about the user's
interest in the retweeted tweet.
To simulate an information overload, we filtered out user week timelines, such
that a user timeline for a given week contains more than 1000 tweets from other
users (see the discussion about information overload impact on
word cloud personalization in {\bf{P4}} in Appendix \ref{app:p4}). We
evaluate only a user's week timelines that contain at least 20 retweets of
a user to measure the effect of personalization methods. To simulate user
preferences, a particular user has to have at least 20 retweets before a given week to derive user preferences.
The pruned evaluation set contains 143 different week timelines for 107 distinct
users. When evaluating Beomap, a two-dimensional topic map (see research paper {\bf{P6}} in Appendix \ref{app:p6}), two different data
collections were employed: (1) TREC2011 microblogging collection \cite{ounis2011overview};
(2) Two tweets collections obtained for task oriented user study. For the
TREC2011 microblogging collection, we retrieved available tweets and
corresponding relevance judgments. The relevance judgments sets were built using
a standard pooling technique \cite{manning2008introduction}. The tweets
relevance was assessed with the three-points scale: (0: irrelevant, 1: relevant
and 2:
highly relevant). We consider both relevant and highly relevant tweets as equally relevant. The TREC2011
microblog corpus consists of fifty distinct queries. For the first user study
task about job search, we retrieved 2.5M tweets. For the second user study task
about the conflict in Ukraine, we obtained 130K tweets. Both collections were
retrieved during the second half of September 2014.
\subsection{Evaluation metrics}
\label{sec:methodologymetrics}
In the following, we describe which types of evaluation metrics are used in this
PhD project. First, we present synthetic metrics, which enabled the simulated
offline evaluation of generated tag and word clouds. Second, we describe evaluation
methodologies when acquiring user opinions about generated word clouds or
a two-dimensional topic map.
\subsubsection{Synthetic metrics}
In research papers {\bf{P1, P2, P3, P4}} and {\bf{P5}}, we employed synthetic
metrics Coverage, Overlap or Relevance, which are more detailed in Section
\ref{sec:evaluationmetrics}.

When evaluating Relevance of word clouds generated from Twitter data in research
papers {\bf{P4}} and {\bf{P5}}, we proposed a novel evaluation methodology, which treats each generated word cloud as a
query. The query, which represents a particular word cloud, is then used to
retrieve tweets. The ratio of relevant tweets within the retrieved result set
then determines Mean Average Precision. The details about the evaluation
methodology are further described in research paper {\bf{P4}} in Appendix
\ref{app:p4}.
\subsubsection{User studies}
In research paper {\bf{P5}} (see Appendix \ref{app:p5}), we acquired user
opinions about generated word clouds through crowdsourcing. In particular, we
selected eight different queries from TREC 2011 microblogging collection
\cite{ounis2011overview}. For each query tweets, we generated a word cloud with
the baseline and the proposed method. Consequently, generated word clouds were
visualized and uploaded into Crowdflower platform (\url{www.crowdflower.com}).
Crowdsourcing users were given word cloud pairs and asked to evaluate
the diversity and relevance of individual word clouds with respect to a given query. For each query, we
provided a link to the related Wikipedia article such that a user could garner
an understanding about the query context. Users rated word cloud pairs on a
scale of 1 to 5 (Rating 1 - word cloud A is very
relevant/diverse to the pertaining query, Rating 3 - both word clouds are equally relevant/diverse to the pertaining query and Rating 5 - word
cloud B is very relevant/diverse to the pertaining query). We altered an
assignment of word clouds with the proposed method to either word
cloud A or B for each query to prevent user bias e.g., to avoid making word
cloud A (with named entities) always more relevant and diverse.

In research paper {\bf{P6}} (see Appendix \ref{app:p6}), we performed a
user evaluation of a developed system BeomapApp (with an integrated two-dimensional
topic map Beomap). Each participant was asked to perform two information
retrieval tasks (see details of tasks in research paper {\bf{P6}}
in Appendix \ref{app:p6}). Prior to the retrieval tasks, a participant was asked
to complete a demographics questionnaire. After each performed task, a user was
asked to complete the USE questionnaire \cite{lund2001measuring} to provide
subjective usability ratings of the system. The questionnaire was extended
with a few additional questions about the following system aspects:
flexible tool for exploration, browsing and analysis of
Twitter data, interesting, familiar with the collection, easy to
browse. Users rated each feature of the interface on a 7-point
Likert scale i.e., 1 for strongly disagree and 7 for strongly
agree. We used a 7-point scale to be consistent with \cite{lund2001measuring}.
To minimize carryover effects in our within-subjects design,
participants performed two tasks with the following conditions.
1) Each task was performed on top of a different tweets
collection. 2) Further, the interfaces and task order were alternated
and counterbalanced among participants (see details in research paper
{\bf{P6}} in Appendix \ref{app:p6}).
